import asyncio
import logging
from logging.handlers import RotatingFileHandler
import os
import sys
import discord
import requests

# https://discordpy.readthedocs.io/en/stable/#manuals
from discord.ext.commands import Bot, CommandError, Context, when_mentioned_or
from dotenv import load_dotenv
from quest_cog import QuestCog

from music_cog import MusicCog
from text_cog import TextCog

formatter = logging.Formatter(fmt='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d]%(name)s %(message)s',
                              datefmt='%Y-%m-%d:%H:%M:%S')
handlers: list[logging.Handler] = [RotatingFileHandler(filename="jodi-the-bard.log", maxBytes=10*1024*1024, backupCount=3),
                                   logging.StreamHandler(stream=sys.stdout)]
for handler in handlers:
    handler.setFormatter(formatter)
logging.basicConfig(handlers=handlers)
# discordLogger = logging.getLogger("discord")
logger = logging.getLogger('jodi')
logger.setLevel(logging.DEBUG)

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

intents = discord.Intents.default()
intents.message_content = True
intents.voice_states = True

bot = Bot(command_prefix=when_mentioned_or("Jodi ", "jodi ", "lingbot ", "LINGBOT ", "Lingbot "),
          description='A music bot that avoids youtube, and utilizes Kodi', intents=intents)


@bot.event
async def on_ready():
    logger.info('Logged in as {0} ({0.id})'.format(bot.user))
    logger.info('------')


@bot.event
async def on_command_error(ctx: Context, error: CommandError):
    logger.error('caught command error %s', str(error))
    await ctx.send(f'Problem found: {str(error)}', delete_after=15)
    await ctx.send_help()


async def main():
    logger.info("Starting main!")
    async with bot:
        try:
            musiccog = MusicCog(bot)
            await bot.add_cog(musiccog)
        except ConnectionError as ce:
            logger.error("No music:\n %s", str(ce))
        except requests.ConnectionError as e:
            logger.error("No music:\n %s", e)
        await bot.add_cog(TextCog(bot))
        try:
            questcog = QuestCog(bot)
            await bot.add_cog(questcog)
        except ConnectionRefusedError as cre:
            logger.error("No quest:\n %s", str(cre))
        logger.debug(TOKEN)
        try:
            await bot.start(TOKEN)
        except KeyboardInterrupt:
            bot.close()

asyncio.run(main())
