import logging
import os
from typing import List
from kodijson import Kodi
from kodi_types import Song, KodiSong


# https://kodi.wiki/view/JSON-RPC_API/v12
# https://github.com/jcsaaddupuy/python-kodijson

logger = logging.getLogger("jodi." + __name__)


class KodiApi:
    def __init__(self, url: str = "http://127.0.0.1:7231/jsonrpc", username: str = "kodi", passwd: str = "kodi") -> None:
        self.kodi_url = url
        url = os.getenv('KODI_URL', url)
        username = os.getenv('KODI_USERNAME', username)
        passwd = os.getenv('KODI_PASSWD', passwd)
        self.kodi = Kodi(url, username, passwd)
        logger.debug(self.kodi.JSONRPC.Ping())
        self.Rescan()

    def Rescan(self) -> None:
        self.kodi.AudioLibrary.Scan()

    def GetMusicRoots(self, label: list = ["Music"]) -> list:
        source = self.kodi.Files.GetSources({"media": "music"})
        roots = []
        if source is not None:
            sources = source['result']['sources']
            for s in sources:
                if label is None or s["label"] in label:
                    roots.append(s)
        logger.debug(roots)
        return roots

    def GetMusicSources(self) -> None:
        thing = self.kodi.Files.GetSources({"media": "music"})
        logger.debug(thing)
        logger.debug(thing["result"])

    def GetDirectory(self, directory: str) -> dict:
        received = self.kodi.Files.GetDirectory({"directory": directory, "properties": ["title", "file"]})
        folders = []
        files: List[Song] = []
        others = []
        for thing in received['result']['files']:
            if 'filetype' in thing:
                if thing['filetype'] == 'directory':
                    folders.append(thing)
                elif thing['filetype'] == 'file':
                    files.append(KodiSong.from_kodi(thing))
                else:
                    others.append(thing)
            else:
                others.append(thing)
        return {'files': files, 'folders': folders, 'others': others}

    # def PlaySong(self, filepath):
    #     players = self.kodi.Player.GetPlayers()
    #     print(players)
    #     print(self.kodi.Player.GetActivePlayers())
    #     print(self.kodi.Player.Open({"item": {"path": filepath}}))
    #     print(self.kodi.Player.PlayPause({"playerid": 0}))
    #     print(self.kodi.Player.GetActivePlayers())

    # returns [{'title', 'file', 'label'}, ...]
    def Search(self, query: str) -> List[Song]:
        songs = self.kodi.AudioLibrary.GetSongs({"properties": ['title', 'file']})['result']['songs']
        results: List[Song] = []
        for song in songs:
            if query.lower() in song['label'].lower():
                results.append(KodiSong.from_kodi(song))
        if len(results) == 0:
            for song in songs:
                if query.lower() in song['file'].lower():
                    results.append(KodiSong.from_kodi(song))
        return results


if __name__ == '__main__':
    from dotenv import load_dotenv
    load_dotenv('../.env')

    kay = KodiApi()
    # roots = kay.GetMusicRoots()
    # print(kay.GetDirectory(roots[0]['file']))
    # print(kay.GetDirectory("/mnt/sda1/files/public/Music/Single's Ward/"))
    # kay.PlaySong("/mnt/sda1/files/public/Music/Single's Ward/Popcorn.wma")
    print(kay.Search("sunshine"))
    print(kay.GetMusicRoots())
    print(kay.GetDirectory(kay.GetMusicRoots()[0]['file']))
