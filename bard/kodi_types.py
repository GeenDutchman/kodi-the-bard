import asyncio
import logging
from typing import ClassVar
from attrs import define, field
from discord import AudioSource, PCMVolumeTransformer, FFmpegPCMAudio
import discord.ext.tasks
from urllib import parse
import os
from yt_tool import YT_tool


logger = logging.getLogger('jodi.kodi_types')
logger.setLevel(logging.DEBUG)

@define
class Song:
    def getTitle(self) -> str:
        pass

    def getAudioSource(self) -> AudioSource:
        pass

    def __str__(self) -> str:
        return self.getTitle()


ffmpeg_options = {
    'options': '-vn'
}


@define
class KodiSong(Song):
    title: str
    file: str
    label: str

    @classmethod
    def from_kodi(cls, kodi_dict: dict):
        return cls(title=kodi_dict['title'], file=kodi_dict['file'], label=kodi_dict['label'])

    def getTitle(self) -> str:
        song_title = "unknown song"
        if self.file and len(self.file) > 0:
            song_title = self.file
        if self.title and len(self.title) > 0:
            song_title = self.title
        elif self.label and len(self.label) > 0:
            song_title = self.label
        return song_title

    def getAudioSource(self) -> AudioSource:
        return PCMVolumeTransformer(FFmpegPCMAudio(self.file, **ffmpeg_options))

    def __str__(self) -> str:
        return f"{self.getTitle()}"


@define(slots=False)
class URLSong(Song):
    backgroundDL: ClassVar[set] = set()
    yt_tool: YT_tool
    url: str
    downloadNow: bool
    downloaded: str = field(init=False, default='')
    fetched: dict = field(init=False, default=None)

    @classmethod
    def from_url(cls, yt_tool: YT_tool, url: str, fetched: dict = None, downloadNow: bool = False):
        this = cls(yt_tool=yt_tool, url=url, downloadNow=downloadNow)
        if fetched is not None:
            this.fetched = fetched
        return this
    
    def checkDownloaded(self) -> bool:
        if self.downloaded:
            return True
        if self.fetched is not None:
            filename = self.yt_tool.ytdl.prepare_filename(self.fetched)
            if os.path.isfile(filename):
                logger.debug("File found at %s", filename)
                self.downloaded = filename
                return True
        return False

    def fetch(self, download_override: bool = False):
        logger.debug("Fetching song, downloadnow %s, download_override %s, downloaded %s", self.downloadNow, download_override, self.downloaded)
        
        should_download = (self.downloadNow or download_override) and not self.checkDownloaded()
        if self.fetched is None:
            logger.debug("fetch_url with shoulddownload %s", should_download)
            self.fetched = self.yt_tool.fetch_url(self.url, should_download)

        # if not self.checkDownloaded() and not should_download:
        #     logger.debug("Starting background download loop")
        #     self.backgroundDownload.start()
        

    async def backgroundDownload(self):
        if not self.checkDownloaded():
            logger.debug("Background download loop started")
            asyncio.get_event_loop().run_until_complete(self.fetch(download_override=True))
            self.fetch(download_override=True)
            logger.debug("Background download loop ended")
        else:
            logger.debug("Already downloaded")

    def getTitle(self) -> str:
        if self.fetched is None:
            self.fetch()
        if self.fetched is None:
            raise f'Unable to fetch {self.url}'
        if 'title' in self.fetched:
            return self.fetched['title']
        return self.url

    def getExpiry(self) -> int:
        if self.fetched is None:
            return None
        result = parse.urlsplit(self.fetched['url'])
        qdict = dict(parse.parse_qsl(result.query))
        if 'expire' in qdict:
            return int(qdict['expire'])
        return None

    def getAudioSource(self) -> AudioSource:
        if self.downloaded:
            return PCMVolumeTransformer(FFmpegPCMAudio(self.downloaded, **ffmpeg_options))
        self.fetch()  # just fetch every time we need the audio source
        if self.fetched is None:
            raise ValueError(f'Unable to fetch {self.url}')

        return PCMVolumeTransformer(FFmpegPCMAudio(self.fetched['url'], **ffmpeg_options))

    def __str__(self) -> str:
        return f"{self.getTitle()}"
