import asyncio
import logging
import os
import pprint
from typing import List

from discord import Embed, Guild, VoiceChannel
from discord.errors import DiscordException
from discord.ext.commands import (Bot, Cog, CommandError, Context, command,
                                  group)
from discord.message import Message
from kodi_types import KodiSong, Song, URLSong

from yt_tool import YT_tool
from kodi_api import KodiApi
from music_queue import DiscordMusicQueue
from navigator import Navigator, PageNavigator, Selector

logger = logging.getLogger('jodi.music_cog')
logger.setLevel(logging.DEBUG)
shallow = pprint.PrettyPrinter(depth=1, compact=True)


class MusicCog(Cog):
    def __init__(self, bot: Bot):
        self.bot: Bot = bot
        self.kodi: KodiApi = None
        try:
            tempKodi: KodiApi = KodiApi()
            self.kodi = tempKodi
        except ConnectionError as ce:
            logger.warning("Running kodi-less...with ConnectionError %s", ce)
        except Exception as nce:
            logger.warning("Running kodi-less...with general Exception %s", nce)
        self.queues: dict = dict()
        path = os.path.expanduser("~/music/Youtube/")
        if self.kodi is not None:
            path = self.kodi.GetMusicRoots()[0]['file'] + 'Youtube/'
        else:
            os.makedirs(path, exist_ok=True)
        self.ytdl = YT_tool(path)

    def getQueue(self, ctx: Context) -> DiscordMusicQueue:
        guild: Guild = ctx.guild
        if guild is None:
            logger.warning("Guild from '%s' context does not exist", ctx.author.name)
            return None
        return self.queues.get(hash(guild))

    async def clearMessage(self, message: Message, delay: float = 5):
        if message is not None:
            try:
                await message.delete(delay=delay)
            except DiscordException:
                logger.error("The message '%s' was not ours to delete", str(message.content))

    @command(name="queue", help="lists the queue")
    async def queue(self, ctx: Context):
        logger.info("%s wants to know about the queue for %s", ctx.author, ctx.guild)
        queue = self.getQueue(ctx)
        if queue is not None and len(queue) > 0:
            status = queue.status()
            pager = status['pager'] if 'pager' in status else None
            embed_dict = status['embed_dict'] if 'embed_dict' in status else None
            await self.selectorAdapter(ctx, pager, embed_dict_provided=embed_dict)
            return
        await ctx.send("No items in queue")
        await self.clearMessage(ctx.message)

    @command(name="loop", help="Makes queue loop")
    async def loop(self, ctx: Context, loop_type: str):
        logger.info("%s wants me to loop %s", ctx.author, loop_type)
        queue = self.getQueue(ctx)
        if queue is not None:
            await queue.setLoop(loop_type, ctx)
            return
        logger.warning("Can find no queue for %s", ctx.author)
        await ctx.send("No queue found! Join a voice channel!")
        await self.clearMessage(ctx.message)

    @loop.error
    async def loop_error(self, ctx: Context, err: CommandError):
        queue = self.getQueue(ctx)
        if queue is None:
            await ctx.reply("No queue found! Join a voice channel!")
            return
        message = str(ctx.author.mention) + " I don't understand that.\n"
        message += "The command is `loop <loop_option>`, with `loop_option` as one of "
        message += str(DiscordMusicQueue.getQueueLoopOptions()) + '\n'
        message += "For example: `loop all`"
        logger.error("%s made a loop error: %s", ctx.author, message)
        await ctx.reply(content=message)

    @command(name="join", help="tells bot to join channel")
    async def join(self, ctx: Context, *, channel: VoiceChannel):
        """Joins a voice channel"""
        logger.info("%s wants me to join %s", ctx.author, channel)

        if ctx.voice_client is not None:
            logger.warning("voice client was none")
            await ctx.voice_client.move_to(channel)

        guild: Guild = ctx.guild
        if guild is None:
            logger.warning("cannot join a None guild!")
            ctx.send("Must join a guild/server!")
            return

        v_client = await channel.connect()
        if self.getQueue(ctx) is None:
            logger.info("Created new queue for '%s'", ctx.guild)
            self.queues[hash(guild)] = DiscordMusicQueue(v_client, name=guild.name)
        else:
            logger.info("Added client to queue for '%s'", ctx.guild)
            self.getQueue(ctx).setClients(v_client)

    @join.error
    async def join_error(self, ctx: Context, err: CommandError):
        logger.error("%s made a join error: %s", ctx.author, err)
        extra_info = ''
        if ctx.guild is not None:
            extra_info = ' Choose from: ' + str([chan.name for chan in ctx.guild.voice_channels])
        await ctx.reply(f"You need to tell me which voice channel to join (case sensitive)!{extra_info} error: {str(err)}")

    async def addToQueue(self, ctx: Context, selected: List[Song] = None, auto_play: bool = True):
        logger.info("%s is adding to the queue", ctx.author)
        if self.getQueue(ctx) is None:
            logger.warn("There is no queue, what is going on?")
            await ctx.send("There is no queue, what is going on?")
            return

        try:
            await self.getQueue(ctx).addMany(selected, ctx, auto_play=auto_play)
        except Exception as e:
            logger.error("Error playing song %s", str(e), exc_info=True)
            await ctx.send('Error playing song')

    # inspired from https://github.com/avrae/avrae/blob/9535f1e97fd1119bf2e11312a4cab35cfe3636ba/utils/functions.py

    async def selectorAdapter(self, ctx: Context, pager: PageNavigator, embed_dict_provided: dict = None, interaction_limit=200):

        def chk(msg: Message):
            valid = {"c"}.union(pager.getAllowedActions())
            return msg.author == ctx.author and msg.channel == ctx.channel and msg.content.lower() in valid

        selectMsg: Message = None
        replyMsg: Message = None
        result = None
        for n in range(interaction_limit):
            this_page = pager.getCurrentPage()
            logger.debug("page: %s", shallow.pformat(this_page))

            embed: Embed = Embed()
            if embed_dict_provided is not None:
                logger.debug("provided %s", shallow.pformat(embed_dict_provided))
                embed = Embed.from_dict(embed_dict_provided)

            if len(embed.title) == 0:
                embed.title = "Many Entries Found"
            embed.add_field(name="How to look around:", value=pager.do_help_str())
            embed.set_footer(text=pager.getPageCountStr())
            logger.debug("Before results: %s", shallow.pformat(embed.to_dict()))

            entry_string = ""
            for item in list(this_page.items()):
                if isinstance(item[1], Song):
                    entry_string += f"**[{item[0]}]** - {item[1].getTitle()}\n"
                elif item[1] is not None and 'file' in item[1] and len(item[1]['file']) > 0:
                    description = ''
                    if not description and 'title' in item[1] and len(item[1]['title']) > 0:
                        description = item[1]['title']
                    elif not description and 'label' in item[1] and len(item[1]['label']) > 0:
                        description = item[1]['label']
                    else:
                        description = item[1]['file']
                    entry_string += f"**[{item[0]}]** - {description}\n"
            embed.add_field(name="Entries", value=entry_string if len(entry_string) > 0 else "No entries found!", inline=False)

            await self.clearMessage(selectMsg, delay=0)
            selectMsg = await ctx.channel.send(embed=embed)

            try:
                replyMsg = await ctx.bot.wait_for('message', timeout=60, check=chk)
            except asyncio.TimeoutError:
                await ctx.channel.send("Selection cancelled", delete_after=15)
                replyMsg = None

            if replyMsg is None:
                break
            else:
                try:
                    did_pager = pager.do(replyMsg.content.lower())
                    if did_pager.results is not None and len(did_pager.results) > 0:
                        result = did_pager.results
                        break
                    await self.clearMessage(replyMsg, delay=0)
                    if did_pager.cancelled:
                        await ctx.channel.send("Selection cancelled", delete_after=15)
                        break
                except PageNavigator.BadAction as e:
                    if replyMsg.content.lower() == 'c':
                        break
                    await ctx.channel.send(f"An error occurred: {str(e)}", delete_after=60)

        try:
            await self.clearMessage(selectMsg)
            await self.clearMessage(selectMsg)
        except DiscordException:
            pass

        return result

    @command(name="explore", help="Look around for a song's availability")
    async def explore(self, ctx: Context):
        """Explores the music filesystem"""
        if self.kodi is None:
            ctx.send("Kodi did not start, so you cannot explore.")
            return
        self.kodi.Rescan()
        path = self.kodi.GetMusicRoots()[0]['file']
        firstItems = self.kodi.GetDirectory(path)['files']
        navigator = Navigator(items=firstItems, newPagesGetter=self.kodi.GetDirectory, pathRoot=path)
        selected = await self.selectorAdapter(ctx, navigator)
        logger.info("Explore selected %s", shallow.pformat(selected))
        if selected is not None:
            await self.addToQueue(ctx, selected, auto_play=False)
        await self.clearMessage(ctx.message)

    # @command(name="play", help="plays from local filesystem")
    @group(name="play", help="plays from local filesystem")
    async def play(self, ctx: Context, *, query: str = ''):
        """Plays a file from the local filesystem"""
        selected = None

        if len(query) == 0:
            if self.getQueue(ctx) is not None:
                await self.getQueue(ctx).play(ctx)
                return
            await ctx.send("We cannot play without a queue or a query!")

        elif len(query) > 0 and not os.path.exists(query) and self.kodi is not None:
            search = self.kodi.Search(query)
            try:
                if len(search) > 1:
                    selector = Selector(search)
                    selected = await self.selectorAdapter(ctx, selector)
                elif len(search) == 1:
                    selected = [search[0]]
                else:
                    raise IndexError
            except Exception as e:
                await ctx.send(f'No song selected, {e}')
                return
        else:
            selected = [KodiSong(query, query, "")]

        if selected is None:
            logger.info("Nothing selected")
            await ctx.send("No song selected")
            return
        for selection in selected:
            if not isinstance(selection, KodiSong):
                logger.warn(f'{selection} is not of the right type')
                await ctx.send('Song was not of the right typeing')
                return
            if not os.path.exists(selection.file):
                logger.warn(f"Song [{selection.file}] not found")
                await ctx.send(f"Song [{selection.file}] not found")
                return

        logger.info("Play selected: %s", shallow.pformat(selected))
        await self.addToQueue(ctx, selected, auto_play=True)
        await self.clearMessage(ctx.message, delay=15)

    @command(name="pause", help="Should pause the music...")
    async def pause(self, ctx: Context):
        """Pauses playback of music"""
        logger.info("%s is pausing the music", ctx.author)
        if self.getQueue(ctx) is None:
            await ctx.send('Error pausing song')
            return
        await self.getQueue(ctx).pause(ctx)
        await self.clearMessage(ctx.message)

    @command(name="resume", help="Should resume the music...")
    async def resume(self, ctx: Context):
        """Resumes playback of music"""
        logger.info("%s is resuming the music", ctx.author)
        if self.getQueue(ctx) is None:
            await ctx.send('Error resuming song')
            return
        await self.getQueue(ctx).resume(ctx)
        await self.clearMessage(ctx.message)

    @command(name='skip', help="Skips to the next song, if available")
    async def skip(self, ctx: Context):
        logger.info("%s is skipping the music", ctx.author)
        if self.getQueue(ctx) is None:
            await ctx.send('Error skipping song')
            return
        await self.getQueue(ctx).skip(ctx)
        await self.clearMessage(ctx.message)

    @command(name='shuffle', help="Will toggle the queue to play shuffled or not")
    async def shuffle(self, ctx: Context, turn: bool):
        logger.info("%s is toggling the shuffle", ctx.author)
        if self.getQueue(ctx) is None:
            await ctx.send('Error toggling shuffle')
            return
        await self.getQueue(ctx).setShuffle(turn, ctx)

    @command(name="search", help="Searches local music")
    async def search(self, ctx: Context, *, query: str):
        if self.kodi is None:
            ctx.send("Kodi did not start, so you cannot search.")
            return
        self.kodi.Rescan()
        await ctx.channel.trigger_typing()
        results = self.kodi.Search(query)

        embed_results_dict = {"title": f"Search results for {query}"}
        selection = await self.selectorAdapter(ctx, Selector(results), embed_dict_provided=embed_results_dict)

        logger.info("Play selected: %s", shallow.pformat(selection))
        if selection is not None:
            await self.addToQueue(ctx, selection, auto_play=False)
        await self.clearMessage(ctx.message)

    async def fromYoutubeDL(self, ctx: Context, url: str, download: bool):
        async with ctx.typing():
            logger.info("%s %s using url: %s", ctx.author, "Downloading" if download else "Streaming", url)
            data = {}
            data = await self.ytdl.fetch_url_async(ctx, self.bot.loop, url, download=download)
            if data is None:
                logger.warning("No data retrieved!")
                return

            if 'entries' in data:
                logger.info("found entries for url: %s", url)
                if isinstance(data['entries'], list):
                    logger.debug("entries found: %s", str([entry['name'] for entry in data['entries'] if 'name' in entry]))
                embed_selector_dict = {"title": "Playlist found"}
                selections = await self.selectorAdapter(ctx, Selector(data['entries']), embed_dict_provided=embed_selector_dict)
                if selections is None:
                    selections = [data['entries'][0]]
                for entry in selections:
                    await self.addToQueue(ctx, selected=[URLSong.from_url(self.ytdl, entry['webpage_url'], entry, download)])
            else:
                logger.info("Adding entry for url: %s", url)
                await self.addToQueue(ctx, selected=[URLSong.from_url(self.ytdl, data["webpage_url"], data, download)])
        # await self.clearMessage(ctx.message, delay=60)

    @command(name="yt", help="play from youtube, takes time to download it", hidden=True)
    async def yt(self, ctx: Context, *, url: str):
        """Plays from a url (almost anything youtube_dl supports)"""
        dlmessage: Message = await ctx.send("This will be a bit, downloading...")
        await self.fromYoutubeDL(ctx, url, download=True)
        await self.clearMessage(dlmessage)

    @command(name="stream", help="play from url, does not predownload", hidden=True)
    async def stream(self, ctx: Context, *, url: str):
        """Streams from a url (same as yt, but doesn't predownload)"""
        await self.fromYoutubeDL(ctx, url, download=False)

    @command(name="volume", help="changes volume")
    async def volume(self, ctx: Context, volume: int):
        """Changes the player's volume"""

        queue = self.getQueue(ctx)
        if queue is None:
            await ctx.send('Error adjusting volume')
            return
        await queue.volume(volume, ctx)
        await self.clearMessage(ctx.message)

    @command(name="stop", help="stops and disconnects bot from voice")
    async def stop(self, ctx: Context):
        """Stops and disconnects the bot from voice"""
        logger.info("%s is stopping the music", ctx.author)
        if self.getQueue(ctx) is None:
            await ctx.send('Error stopping song')
            return
        await self.getQueue(ctx).stop(ctx)
        await self.clearMessage(ctx.message, delay=60)

    @play.before_invoke
    @volume.before_invoke
    @stop.before_invoke
    @resume.before_invoke
    @pause.before_invoke
    @yt.before_invoke
    @stream.before_invoke
    async def ensure_voice(self, ctx: Context):
        foundQueue = self.getQueue(ctx)
        logger.debug(f"ensuring voice for {ctx.author}, vc {ctx.voice_client}, queue {foundQueue}, guild {ctx.guild}")
        if self.getQueue(ctx) is None:
            logger.debug("no queue found")
            if ctx.voice_client is None:
                logger.debug("no context voice client found")
                if ctx.guild is None:
                    logger.error('Cannot ensure voice for no guild')
                    await ctx.send('You are not in a guild')
                    raise CommandError("Author is not connected to a guild")
                if ctx.author.voice:
                    v_client = await ctx.author.voice.channel.connect()
                    if self.getQueue(ctx) is None:
                        logger.debug("queue not found, creating new queue")
                        self.queues[hash(ctx.guild)] = DiscordMusicQueue(v_client, name=ctx.guild.name)
                    else:
                        logger.debug("queue found")
                else:
                    logger.warning("author voice not found")
                    await ctx.send("You are not connected to a voice channel.")
                    raise CommandError("Author not connected to a voice channel.")
            else:
                logger.debug("context voice client found, using to create queue")
                v_client = await ctx.voice_client.connect(timeout=60, reconnect=True)
                self.queues[hash(ctx.guild)] = DiscordMusicQueue(v_client, name=ctx.guild.name)
        else:
            logger.debug("queue found")
