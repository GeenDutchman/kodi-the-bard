import asyncio
import uuid
import logging
from typing import List
import discord
from discord import VoiceClient, ClientException
from kodi_types import Song, URLSong
from navigator import Pager
import random

logger = logging.getLogger("jodi." + __name__)
ffmpeg_options = {
    'options': '-vn'
}


class DiscordMusicQueue:
    def __init__(self, v_client: VoiceClient, name: str = None):
        self.uuid = uuid.uuid4()
        self.name = name
        logger.info("Created logger for new queue: %s%s", self.name + ':' if self.name is not None else '', str(self.uuid))
        self.v_client = v_client
        self.queue: List[Song] = []
        self.queue_loop = "off"
        self.shuffle: bool = False
        self.current_song: Song = None
        self.current_volume = 50
        self.logger = logger.getChild(str(self.name.replace(' ', '_').replace('.', '_') if self.name is not None and self.name != '' else self.uuid))
        self.logger.info("Created logger for new queue: %s%s", self.name + ':' if self.name is not None else '', str(self.uuid))

    @classmethod
    def getQueueLoopOptions(cls):
        return set(["off", "single", "all"])

    def setClients(self, v_client: VoiceClient):
        if v_client is not None:
            self.v_client = v_client

    def __len__(self):
        return len(self.queue) + (1 if self.current_song is not None else 0)

    async def setLoop(self, option: str, t_client: discord.abc.Messageable):
        option = option.lower()
        if option not in DiscordMusicQueue.getQueueLoopOptions():
            await t_client.send(f"Cannot set loop to: '{option}', correct options are: {DiscordMusicQueue.getQueueLoopOptions()}")
            return
        self.queue_loop = option
        await t_client.send(f"Queue loop set to: {self.queue_loop}", delete_after=15)

    async def toggleShuffle(self, t_client: discord.abc.Messageable):
        self.shuffle = not self.shuffle
        await t_client.send("Shuffle set to: " + ("on" if self.shuffle else "off"), delete_after=15)
        return self.shuffle

    async def setShuffle(self, to_set: bool, t_client: discord.abc.Messageable):
        self.shuffle = to_set
        await t_client.send("Shuffle set to: " + ("on" if self.shuffle else "off"), delete_after=15)
        return self.shuffle

    async def stop(self, t_client: discord.abc.Messageable):
        self.logger.info(f"Stopping {self}")
        self.v_client.pause()
        self.v_client.stop()
        await self.v_client.disconnect()
        self.v_client.source = None
        self.current_song = None
        await t_client.send("Stopped, cleared current song", delete_after=60)

    async def clear(self, t_client: discord.abc.Messageable):
        self.logger.info(f"Clearing {self}")
        self.queue = []
        await t_client.send("Queue cleared, current song kept", delete_after=60)

    def getCurrentSong(self):
        return self.current_song

    def popNextSong(self):
        if len(self.queue) == 0:
            return None
        nextSong = None
        while len(self.queue) > 0 and nextSong is None:
            if not self.shuffle:
                nextSong = self.queue.pop(0)
            else:
                index = random.randint(0, len(self.queue))
                nextSong = self.queue.pop(index)
        return nextSong

    async def add(self, song: Song, t_client: discord.abc.Messageable, auto_play: bool = True):
        if song is None:
            return
        self.queue.append(song)
        await t_client.send(f"'{song.getTitle()}' added to the queue")
        if auto_play is True and not self.v_client.is_playing():
            await self.play(t_client)

    async def addMany(self, songs: List[Song], t_client: discord.abc.Messageable, auto_play: bool = True):
        if songs is None or len(songs) == 0:
            return
        self.queue.extend(songs)
        outstring = f"'{songs[0].getTitle()}' "
        if len(songs) > 1:
            outstring += f"and {len(songs) - 1 } more "
        outstring += "added to the queue"
        await t_client.send(outstring)
        if auto_play is True and not self.v_client.is_playing():
            await self.play(t_client)

    async def skip(self, t_client: discord.abc.Messageable):
        if len(self) > 0:
            # self.v_client.stop()
            source = self.trigger_next(single_override=True)
            if source is not None:
                old_source = self.v_client.source
                self.v_client.source = source
                if old_source is not None:
                    old_source.cleanup()
                # self.v_client.resume()
                await t_client.send(f"Skipped...now playing '{self.current_song.getTitle()}'", delete_after=15)
                return
        await t_client.send("No more songs")

    async def resume(self, t_client: discord.abc.Messageable):
        await self.play(t_client)

    async def pause(self, t_client: discord.abc.Messageable):
        if not self.v_client.is_connected():
            await self.v_client.connect(reconnect=True, timeout=60)
        if self.v_client.is_playing():
            self.v_client.pause()
            await t_client.send("Pausing", delete_after=60)
        # silent else

    def trigger_next(self, song_override: Song = None, single_override: bool = False):
        self.logger.debug(f"Next triggered, song_override: {song_override} single_override: {single_override}")
        if song_override is not None:
            self.current_song = song_override
        else:
            if self.queue_loop == 'single' and not single_override and self.current_song is not None:
                self.logger.debug("Next Triggered: Arguments met to keep current song")
                self.current_song = self.current_song
            elif self.queue_loop == 'all':
                self.logger.debug("Next Triggered: Keeping current song in the queue")
                self.queue.append(self.current_song)
                self.current_song = self.popNextSong()
            else:
                self.logger.debug("Next Triggered: dropping current song")
                self.current_song = self.popNextSong()

        self.logger.debug(f"Next triggered song: {self.current_song}")
        if self.current_song is None:
            return None

        source = self.current_song.getAudioSource()
        return source

    async def play(self, t_client: discord.abc.Messageable):
        if not self.v_client.is_connected():
            await self.v_client.connect(reconnect=True, timeout=60)
        if self.v_client.is_playing():
            return
        if self.v_client.is_paused():
            await t_client.send("Resuming", delete_after=10)
            self.v_client.resume()
            return

        try:
            def afterProcess(error):
                self.logger.info("song '%s' finalized, source: %s", self.getCurrentSong(), self.v_client.source)
                nsource = None
                if error is not None:
                    err_string = f"There was a playback error {error}"
                    self.logger.error(err_string)
                    coro = t_client.send(err_string)
                    fut = asyncio.run_coroutine_threadsafe(coro, self.v_client.loop)
                    try:
                        fut.result()
                    except Exception as sendErr:
                        self.logger.error("Error sending the error: %s", sendErr)

                if not self.v_client.is_connected() or self.v_client.is_paused():
                    return

                nsource = self.trigger_next()
                if nsource is not None:
                    self.logger.info("Next song found: %s", self.getCurrentSong())
                    self.v_client.play(nsource, after=afterProcess)
                    self.v_client.source.volume = self.current_volume
                else:
                    self.logger.info("No next triggered")

            source = self.trigger_next()
            if source is None:
                await t_client.send("No more songs! Add some!", delete_after=60)
                return
            self.v_client.play(source, after=afterProcess)
            self.v_client.source.volume = self.current_volume
            await t_client.send(f"Now playing '{self.current_song.getTitle()}'", delete_after=60)

        except TypeError as te:
            self.logger.error(f"TypeError Playing: {te}")
            await t_client.send(f"Error: {te}")
        except ClientException as ce:
            self.logger.error(f"ClientException Playing: {ce}")
            await t_client.send(f"Error: {ce}")
        except BaseException as err:
            self.logger.error(f"Unexpected error while playing: {err=}, {type(err)=}")
            raise

    async def volume(self, level: int, t_client: discord.abc.Messageable):
        if level < 0:
            await t_client.send("Volume cannot be set to a negative number! Ignoring.", delete_after=15)
            return
        elif level > 100:
            await t_client.send("Volume cannot be more than 100% !  Ignoring.", delete_after=15)
            return
        self.v_client.source.volume = level if level <= 100 else 100
        self.current_volume = self.v_client.source.volume
        await t_client.send("Changed volume to {}%".format(level), delete_after=15)

    def status(self):
        status_dict = dict()
        status_dict["pager"] = Pager(self.queue)
        status_dict["random"] = self.shuffle
        status_dict["loop"] = self.queue_loop
        status_dict["current_song"] = self.current_song
        embed_dict = dict()
        embed_dict['title'] = "MusicQueue status"
        embed_dict['fields'] = []
        embed_dict['fields'].append({'name': "Current Song", 'value': 'No song' if self.current_song is None else self.current_song.getTitle()})
        embed_dict['fields'].append({'name': "Loop", 'value': self.queue_loop})
        embed_dict['fields'].append({'name': "Random", 'value': 'on' if self.shuffle else 'off'})
        embed_dict['fields'].append({'name': "Playing?", 'value': str(self.v_client.is_playing())})
        embed_dict['fields'].append({'name': "Volume", 'value': str(self.current_volume)})
        status_dict["embed_dict"] = embed_dict
        return status_dict

    def __repr__(self) -> str:
        return (f'name:{self.name} ' if self.name is not None else '') + f'uuid:{str(self.uuid)} len:{len(self)}'
