
import logging
import os
from typing import Iterable

rootLoggerName = "jodi." + __name__

logger = logging.getLogger(rootLoggerName)


class PNResponse:
    def __init__(self) -> None:
        self.done: bool = False
        self.cancelled: bool = False
        self.command: str = ''
        self.results: list = None

    def __str__(self) -> str:
        toReturn = f"done: {self.done} "
        toReturn += f"cancelled: {self.cancelled} "
        toReturn += f"command: {self.command} "
        toReturn += f"results: {self.results} "
        return toReturn

    @classmethod
    def merge(cls, one: 'PNResponse', two: 'PNResponse') -> 'PNResponse':
        logger.debug("Why is one %s and two %s, so?", str(one), str(two))
        merged = PNResponse()
        if one is None and two is None:
            return merged
        if one is None and two is not None:
            return two
        if one is not None and two is None:
            return one

        if one.done or two.done:
            merged.done = True
        if one.cancelled or two.cancelled:
            merged.cancelled = True
        if one.command == '' and two.command != '':
            merged.command = two.command
        elif one.command != '' and two.command == '':
            merged.command = one.command
        elif one.command == two.command:
            merged.command = one.command
        else:
            merged.command = one.command + '/' + two.command
        if one.results is not None:
            merged.results = one.results
        if two.results is not None:
            merged.results = two.results if merged.results is None else merged.results + two.results
        logger.debug("merged to %s", merged)
        return merged


class PageNavigator:
    class BadAction(BaseException):
        '''Raised when a bad action is taken'''
        # pass

        def __init__(self, message: str):
            super().__init__(f"A bad action was taken: {message}")

    class DidNothing(BadAction):
        '''Raised when nothing was done'''

        def __init__(self, notaction: str):
            super().__init__(f"Could not perform '{notaction}'")

    def do_help_str(self) -> str:
        pass

    def do(self, action: str) -> PNResponse:
        pass

    def getCurrentPage(self) -> dict:
        pass

    def getPageCount(self) -> tuple:
        pass

    def getPageCountStr(self) -> str:
        pass

    def getAllowedActions(self) -> set:
        pass

    def __len__(self) -> int:
        pass


class Pager(PageNavigator):
    class EndReached(PageNavigator.BadAction):
        '''Raised when an end is reached'''

        def __init__(self, start: bool) -> None:
            if start:
                super().__init__("Already on first page")
            else:
                super().__init__("Already on last page")

    def __init__(self, items: Iterable, page_size: int = 10):
        self.pages = self.makePages(items, page_size)
        self.pageIndex = 0
        self.length = len(items)
        self.logger = logging.getLogger(rootLoggerName + '.' + __name__)

    def makePages(self, iterable: Iterable, page_size: int = 10, positive_count: bool = True):
        pages = []
        for i, item in enumerate(iterable):
            if i % page_size == 0:
                pages.append(dict())
            index = i + 1
            if not positive_count:
                index *= -1
            pages[-1].update({str(index): item})
        return pages

    def getCurrentPage(self) -> dict:
        if len(self.pages) == 0:
            return dict()
        return dict(self.pages[self.pageIndex])

    def getAllPages(self) -> dict:
        if len(self.pages) == 0:
            return dict()
        toReturn = dict()
        for i in range(len(self.pages)):
            toReturn.update(self.pages[i])
        return toReturn

    def getPageCount(self) -> tuple:
        return self.pageIndex + 1, len(self.pages)

    def getPageCountStr(self) -> str:
        index, total = self.getPageCount()
        return f"Page: {index}/{total}" if total > 0 else ""

    def do_help_str(self) -> str:
        help_str = f"This {self.__class__.__name__} helps you see many entries.\n"
        help_str += "You can type 'c' to stop. "
        if len(self.pages) > 1:
            help_str += "Type 'n' to see the next page of results, or 'p' for the previous page.\n"
        return help_str

    def getAllowedDirections(self) -> set:
        return {"n", "p", "c"}

    def getAllowedActions(self) -> set:
        return self.getAllowedDirections()

    def do(self, direction: str) -> PNResponse:
        response = PNResponse()
        direction = direction.lower()
        response.command = direction
        self.logger.info("Pager do: %s", direction)
        if direction not in self.getAllowedActions():
            e = PageNavigator.BadAction(f"{direction} not allowed")
            self.logger.error(e)
            raise e
        if direction == "n":
            if self.pageIndex + 1 < len(self.pages):
                self.pageIndex += 1
                response.done = True
            else:
                e = Pager.EndReached(False)
                self.logger.error(e)
                raise e
        elif direction == "p":
            if self.pageIndex - 1 >= 0:
                self.pageIndex -= 1
                response.done = True
            else:
                e = Pager.EndReached(True)
                self.logger.error(e)
                raise e
        elif direction == "c":
            response.done = True
            response.cancelled = True
        return response

    def __len__(self) -> int:
        return self.length


class Selector(Pager):
    class BadSelection(Pager.BadAction):
        '''Raised when you cannot select that option'''
        pass

    def __init__(self, items: Iterable, page_size: int = 10, force_single=True):
        super().__init__(items=items, page_size=page_size)
        self.force_single = force_single
        self.logger = logging.getLogger(rootLoggerName + '.' + __name__)

    def getAllowedSelections(self) -> set:
        return {str(v) for v in self.getCurrentPage().keys()}.union({'all', 'page'})

    def getAllowedActions(self) -> set:
        return super().getAllowedActions().union(self.getAllowedSelections())

    def do_help_str(self) -> str:
        help_str = super().do_help_str()
        help_str += "Type in a number to select an entry.  "
        help_str += "Type in a comma separated list of numbers to select more than one entry.  "
        help_str += "Type 'page' to select all in the current page, or type 'all' to select every entry\n"
        return help_str

    def do(self, selection: str) -> PNResponse:
        response = PNResponse()
        selection = selection.lower()
        response.command = selection

        response = PNResponse.merge(response, super().do(selection))
        self.logger.debug("merged from pager %s", response)

        if not response.done:
            self.logger.info("Selector do: %s", selection)
            if self.force_single and len(self.getCurrentPage()) == 1:
                page = self.getCurrentPage()
                response.done = True
                response.results = [page[list(page.keys())[0]]]
                return response

            if selection == 'page' or selection == 'all':
                page = self.getCurrentPage() if selection == 'page' else self.getAllPages()
                response.done = True
                response.results = [entry for entry in page.values()]
                return response

            selectors = [x.strip() for x in selection.split(',')]
            response.results = []
            for selector in selectors:
                if selector in self.getAllowedSelections():
                    response.results.append(self.getCurrentPage()[selection])
                else:
                    e = Selector.BadSelection(f"{selector} is not allowed, choose from {self.getAllowedSelections()}")
                    self.logger.error(e)
                    raise e
            response.done = True

        return response


class Navigator(Selector):
    class BadNavigation(Pager.BadAction):
        '''Raised when you navigate badly'''
        pass

    def __init__(self, items: Iterable, page_size: int = 10, newPagesGetter=None, pathRoot: os.PathLike = "/"):
        self.newPagesGetter = newPagesGetter
        self.currentPath = os.path.normpath(pathRoot)
        self.pathRoot = pathRoot
        super().__init__(items=items, page_size=page_size)
        self.directory_pages = []
        self.directory_index = 0
        if self.newPagesGetter is not None and callable(self.newPagesGetter):
            retrieved = self.newPagesGetter(self.currentPath)
            if 'folders' in retrieved:
                self.directory_pages = self.makePages(retrieved['folders'], positive_count=False)
                self.directory_index = 0
        self.logger = logging.getLogger(rootLoggerName + '.' + __name__)

    def getCurrentPage(self) -> dict:
        mainPage = super().getCurrentPage()
        mainPage.update(self.getCurrentDirectories())
        return mainPage

    def getCurrentDirectories(self):
        if len(self.directory_pages) == 0:
            return dict()
        return dict(self.directory_pages[self.directory_index])

    def getAllowedNavigations(self):
        return {"u", "nd", "pd"}.union({str(v) for v in self.getCurrentDirectories().keys()})

    def getAllowedActions(self):
        return super().getAllowedActions().union(self.getAllowedNavigations())

    def navigate(self):
        if self.newPagesGetter is not None and callable(self.newPagesGetter):
            retrieved = self.newPagesGetter(self.currentPath)
            if 'folders' in retrieved:
                self.directory_pages = self.makePages(retrieved['folders'], positive_count=False)
                self.directory_index = 0
            if 'files' in retrieved:
                self.pages = self.makePages(retrieved['files'])
                self.pageIndex = 0

    def do_help_str(self) -> str:
        help_str = super().do_help_str()
        if len(self.getCurrentDirectories()) > 0:
            help_str += "Type in a negative number to change directories. "
        if not os.path.samefile(self.currentPath, self.pathRoot):
            help_str += "Type 'u' to go up a directory. "
        if len(self.directory_pages) > 1:
            help_str += "Type 'nd' to see the next page of directories, and 'pd' to see the previous page of directories. \n"
        return help_str

    def getPageCountStr(self) -> str:
        return super().getPageCountStr() + f" Directory: {self.directory_index + 1}/{len(self.directory_pages)}" if len(self.directory_pages) > 0 else ""

    def do(self, navigation: str) -> PNResponse:
        navigation = navigation.lower()
        response = PNResponse()
        response.command = navigation
        self.logger.info("Navigator do: %s", navigation)
        if navigation in self.getAllowedNavigations():
            if navigation == "u":
                if not os.path.samefile(self.currentPath, self.pathRoot):
                    self.logger.debug(self.currentPath)
                    self.currentPath = os.path.dirname(os.path.normpath(self.currentPath))
                    self.logger.debug(self.currentPath)
                    self.navigate()
                    response.done = True
                    return response
                else:
                    e = Navigator.BadNavigation("Cannot navigate above root directory")
                    self.logger.error(e)
                    raise e
            if navigation == "nd":
                if self.directory_index + 1 < len(self.directory_pages):
                    self.directory_index += 1
                    response.done = True
                    return response
                else:
                    e = Pager.EndReached(False)
                    self.logger.error(e)
                    raise e
            if navigation == "pd":
                if self.directory_index - 1 >= 0:
                    self.directory_index -= 1
                    response.done = True
                    return response
                else:
                    e = Pager.EndReached(True)
                    self.logger.error(e)
                    raise e

        response = PNResponse.merge(response, super().do(navigation))
        self.logger.debug("merged from selector %s", response)

        if response.results is not None and len(response.results) == 1:
            result = response.results[0]
            if result is not None and 'filetype' in result and result['filetype'] == 'directory' and 'file' in result:
                self.currentPath = os.path.normpath(result['file'])
                self.navigate()
                response.results = []
                response.done = True
                # do not return the selection, because we consumed it
        # else no selection made
        return response
