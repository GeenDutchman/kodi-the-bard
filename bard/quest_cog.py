import asyncio
import os
import random
from html.parser import HTMLParser
from telnetlib import Telnet
from typing import Union
from dotenv import load_dotenv
from discord import Member, User
from discord.ext import tasks
from discord.ext.commands import Bot, Cog, Context, group, CommandError

# with Telnet('localhost', 3001) as tn:
#     bytesss = 'a'
#     while bytesss:
#         bytesss = tn.read_until(b'\n', 10)
#         a_string = bytesss.decode('utf-8')
#         print(a_string)

load_dotenv()
QUEST_URL = os.getenv('QUEST_URL', 'localhost')
QUEST_PORT = int(os.getenv('QUEST_PORT', '3001'))


class DynamicColorizer(HTMLParser):
    def __init__(self):
        super().__init__()
        self.colors: dict[str, str] = dict()
        self.colors_list = [str(x) for x in range(31, 37)]
        self.colors_index = 0
        self.color_stack = []
        self.colored_str = ''

    def fetch_fresh_color(self):
        self.colors_index += 1
        self.colors_index = self.colors_index % len(self.colors_list)
        return self.colors_list[self.colors_index]

    def handle_starttag(self, tag, attrs):
        if tag not in self.colors:
            self.colors[tag] = self.fetch_fresh_color()
        self.color_stack.append(self.colors[tag])

    def handle_endtag(self, tag):
        if len(self.color_stack):
            self.color_stack.pop()

    def handle_data(self, data):
        if len(self.color_stack) > 0:
            self.colored_str += f"\033[{self.color_stack[-1]}m"
        else:
            self.colored_str += "\033[0m"
        self.colored_str += str(data)

    def feed(self, data: str) -> str:
        self.colored_str = ''
        super().feed(data)
        return self.colored_str


# dc = DynamicColorizer()
# print(dc.feed("<command>boolface maguffin</command>"))
# print(dc.feed("xoopie<command>chapow<cow>moo</cow>hi<alpha>mooshi mooshi</alpha>noogie</command>narx"))


class QuestCog(Cog):
    class TelnetDeets:
        def __init__(self, telnet: Telnet, member: Member, colorizer: DynamicColorizer):
            self.telnet: Telnet = telnet
            self.member: Member = member
            self.colorizer: DynamicColorizer = colorizer
            self.lock = asyncio.Lock()

        async def to_server(self, message: str):
            if message[-1] != '\n':
                message += '\n'
            async with self.lock:
                byte_array = message.encode('utf-8')
                self.telnet.write(byte_array)

        @tasks.loop(seconds=1)
        async def monitor(self):
            async with self.lock:
                if self.telnet is not None and self.member is not None:
                    byte_array = b'a'
                    recv_str = ''
                    while byte_array:
                        byte_array = self.telnet.read_until(b'\n', 0.5)
                        recv_str += byte_array.decode('utf-8')
                    if recv_str != '':
                        recv_str = "```ansi\n" + self.colorizer.feed(recv_str) + "\n```"
                        print(recv_str)
                        await self.member.send(recv_str)

    def __init__(self, bot: Bot):
        self.bot: Bot = bot
        self.tnmap: dict[Member, QuestCog.TelnetDeets] = dict()
        self.checkConnection()

    def checkConnection(self):
        try:
            tn = Telnet(QUEST_URL, QUEST_PORT)

            def listen():
                byte_array = b'a'
                recv_str = ''
                while byte_array:
                    byte_array = tn.read_until(b'\n', 0.5)
                    recv_str += byte_array.decode('utf-8')
            listen()
            tn.write(b'exit')
            listen()
            tn.close()
        except ConnectionRefusedError as e:
            raise e

    def checkMember(self, member) -> bool:
        return member in self.tnmap

    def gen_rand_name(self) -> str:
        fnames = ["Shreet", "Noms", "Tuffins", "Mlaki", "Zoomer", "Notta"]
        lnames = ["Boomvanger", "Naaktgeboren", "Adventurer", "Machine", "Reklum", "Efteling"]
        return random.choice(fnames) + random.choice(lnames) + str(random.randrange(10, 100))

    @group(name="quest", help="Start Ibaif!", hidden=True)
    async def quest(self, ctx: Context, *args):
        member: Union[User, Member] = ctx.author
        if member not in self.tnmap:
            async with ctx.typing():
                try:
                    telnetdeets = QuestCog.TelnetDeets(Telnet(QUEST_URL, QUEST_PORT), member, DynamicColorizer())
                except ConnectionRefusedError:
                    ctx.reply("Cannot connect to quest giver, is it running?  In the expected place?")
                    return
                await telnetdeets.monitor()
            async with ctx.typing():
                username = member.nick if hasattr(member, "nick") else member.name
                if username is None:
                    username = self.gen_rand_name()
                username = str(username).replace(' ', '_')
                cmd = f'create "{username}" with "{member.id}"'
                await member.send(cmd)
                await telnetdeets.to_server(cmd)
                await telnetdeets.monitor()
            self.tnmap[member] = telnetdeets
            telnetdeets.monitor.start()
            return
        print(args)
        cmd = ' '.join(args)
        if 'create' in cmd:
            await ctx.reply("That is automated for you, don't worry!")
            return
        if len(args) == 0 or cmd == '' or cmd == ' ':
            cmd = "help"
        async with ctx.typing():
            try:
                await self.tnmap[member].to_server(cmd)
                await self.tnmap[member].monitor()
            except EOFError:
                await member.send("Connection to quest closed")
                self.tnmap.pop(member, None)

    @quest.error
    async def quest_error(self, ctx: Context, err: CommandError):
        await ctx.reply(f"There was a quest error {str(err)}")
