from discord import Member
from discord.ext.commands import Cog, Context, command


class TextCog(Cog):
    def __init__(self, bot):
        self.bot = bot

    @command(name="Greet", help="I greet you.")
    async def greet(self, ctx: Context, member: Member):
        """Says when a member joined."""
        await ctx.send('{0.name} joined in {0.joined_at}'.format(member))

    @command(name="prefix", help="Change the prefix")
    async def prefix(self, ctx: Context, char: str):
        self.bot.command_prefix = char
        await ctx.send('{0.name} set as the new prefix'.format(char))

    @command(name="ping", help="Test connectivity")
    async def ping(self, ctx: Context):
        await ctx.send(f"@{ctx.author.name} I say pong")
