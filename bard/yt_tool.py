import asyncio
import logging
import os
import pprint
import yt_dlp

from discord.ext.commands import Context

shallow = pprint.PrettyPrinter(depth=1, compact=True)

yt_dlp.utils.bug_reports_message = lambda: ''


class YT_tool:
    def __init__(self, outdir: str) -> None:
        self.logger = logging.getLogger('jodi.yt_tool')
        self.logger.setLevel(logging.DEBUG)
        self.ytdl_format_options = {
            'format': 'bestaudio/best',
            'outtmpl': os.path.join(outdir, '%(id)s-%(channel)s_%(title)s.%(ext)s'),
            'restrictfilenames': True,
            'noplaylist': True,
            'nocheckcertificate': True,
            'ignoreerrors': True,
            'logtostderr': False,
            'quiet': True,
            'no_warnings': True,
            'default_search': 'auto',
            'source_address': '0.0.0.0',  # bind to ipv4 since ipv6 addresses cause issues sometimes
            'extract_audio': True,
            'audio_format': 'mp3',
            'allow_unplayable_formats': False,
            'logger': self.logger,
            'compat_opts': ('no-youtube-unavailable-videos',)
        }
        self.ytdl = yt_dlp.YoutubeDL(self.ytdl_format_options)

    def __enter__(self):
        return self.ytdl.__enter__()

    def __exit__(self, *args):
        return self.ytdl.__exit__(args)

    def fetch_url(self, url: str, download: bool) -> dict:
        self.logger.info("Looking up url: %s, downloading: %s", url, str(download))
        try:
            extracted = self.ytdl.extract_info(url, process=True, download=download)
            data = self.ytdl.sanitize_info(extracted)
            selected_keys = ['entries', 'age_limit', 'audio_ext', 'channel', 'description', 'duration_string', 'extractor',
                             'filesize', 'fulltitle', 'protocol', 'title', 'uploader', 'url', 'has_drm', 'webpage_url']
            if isinstance(data, dict) and len(data.keys()) > len(selected_keys):
                self.logger.info("Found: \n%s (fields hidden)", shallow.pformat({key: data[key] for key in selected_keys if key in data}))
            else:
                self.logger.info("Found: \n%s", pprint.pformat(data))
            return data
        except yt_dlp.DownloadError as ytdlde:
            self.logger.error("Download error: %s", ytdlde, exc_info=1)
            return None
        except BaseException as err:
            self.logger.error(f"Unexpected {err=}, {type(err)=}")
            raise
        except:
            self.logger.error("An error has occured, but we have no information on it")
            return None

    async def fetch_url_async(self, ctx: Context, loop: asyncio.AbstractEventLoop, url: str, download: bool) -> dict:
        useloop = loop or asyncio.get_event_loop()
        data = await useloop.run_in_executor(None, lambda: self.fetch_url(url, download))
        if (ctx is not None) and (data is None):
            await ctx.send(f"There was an error accessing {url}.  It's logged, don't worry. Perhaps try a different url?")
        return data

    async def prep(self, data):
        return self.ytdl.prepare_filename(data)
