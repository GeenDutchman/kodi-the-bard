cd $HOME/kodi-the-bard/bard
git pull --rebase

previous=`ps aux | grep jodi.py | grep python3`
echo $previous
if [ -n "$previous" ]; then
        echo "Found previous $previous, killing"
        kill `echo "$previous" | awk '{print $2}'`
else
        echo "Previous run not found"
fi

poetry install
poetry run python3 jodi.py


