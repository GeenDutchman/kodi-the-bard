from bard import kodi_api


class Test_KodiApi():

    def test_search(self):
        results = kodi_api.KodiApi().Search("sunshine")
        print(results)
        for song in results:
            print(song)
            assert "sunshine".lower() in song.getTitle().lower()
        # file, label, songid, title

    def test_get_directory(self):
        kodi = kodi_api.KodiApi()
        path = kodi.GetMusicRoots()  # [{file, label}]
        print(path)
        items = kodi.GetDirectory(path[0]['file'])  # {files: [{file, filetype, label, title, type}], folders: {}, others[]}
        print(items)
