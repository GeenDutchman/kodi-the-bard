from bard.navigator import PNResponse, Pager
import pytest


class Test_PNResponse:

    def test_merge_done(self):
        a = PNResponse()
        assert a.done is False
        b = PNResponse()
        b.done = True
        assert b.done is True
        c = PNResponse.merge(a, b)
        assert c.done is True

    def test_merge_none(self):
        result = PNResponse.merge(None, None)
        assert result is not None
        has_cmd = PNResponse()
        has_cmd.command = "I am unique"
        assert has_cmd.command in str(has_cmd)
        result = PNResponse.merge(has_cmd, None)
        assert has_cmd.command in str(result)
        result = PNResponse.merge(None, has_cmd)
        assert has_cmd.command in str(result)

    def test_merge_command(self):
        a = PNResponse()
        a.command = "I am unique"
        b = PNResponse()
        b.command = "No, I am!"
        c = PNResponse()
        result = PNResponse.merge(c, c)
        assert result.command == ""
        result = PNResponse.merge(c, a)
        assert a.command in str(result)
        result = PNResponse.merge(a, c)
        assert a.command in str(result)
        result = PNResponse.merge(c, b)
        assert b.command in str(result)
        result = PNResponse.merge(b, c)
        assert b.command in str(result)
        result = PNResponse.merge(a, b)
        assert a.command in str(result)
        assert b.command in str(result)
        result = PNResponse.merge(b, a)
        assert a.command in str(result)
        assert b.command in str(result)
        result = PNResponse.merge(a, a)
        assert a.command in str(result)
        assert '/' not in result.command

    def test_merge_results(self):
        a = PNResponse()
        a.results = ['a']
        b = PNResponse()
        b.results = ['b']
        c = PNResponse()
        result = PNResponse.merge(a, c)
        assert 'a' in result.results
        assert len(result.results) == 1
        result = PNResponse.merge(c, a)
        assert 'a' in result.results
        assert len(result.results) == 1
        result = PNResponse.merge(a, b)
        assert 'a' in result.results
        assert 'b' in result.results
        assert len(result.results) == 2
        result = PNResponse.merge(b, a)
        assert 'a' in result.results
        assert 'b' in result.results
        assert len(result.results) == 2


class Test_Pager:
    #  https://stackoverflow.com/questions/46040478/how-to-test-a-class-inherited-methods-in-pytest

    def check_page(self, page_dict: dict, expectedKey=None):
        if expectedKey is not None:
            assert expectedKey in page_dict
        for key in page_dict:
            assert int(key) == int(page_dict[key])

    def test_make_pages(self):
        pageSize: int = 5
        items = [a + 1 for a in range(15)]
        pager = Pager(items, pageSize)
        assert len(pager.pages) == int(len(items) / pageSize)
        for page_dict in pager.pages:
            self.check_page(page_dict=page_dict)

    def test_do_previous(self):
        pager = Pager([1, 2], 1)
        numPages = pager.getPageCount()[1]
        pager.pageIndex = numPages - 1

        with pytest.raises(Pager.EndReached):
            for pageIdxPlus in reversed(range(numPages + 1)):
                page = pager.getCurrentPage()
                self.check_page(page, str(pageIdxPlus))

                do_result = pager.do('p')
                assert do_result.done
                assert not do_result.cancelled
                assert do_result.command == 'p'
                assert do_result.results is None

    def test_do_bad_previous(self):
        pager = Pager([1], 1)

        with pytest.raises(Pager.EndReached):
            for _ in range(pager.getPageCount()[1] + 1):
                pager.do('p')

    def test_do_next(self):
        pager = Pager([1, 2], 1)
        numPages = pager.getPageCount()[1]

        with pytest.raises(Pager.EndReached):
            for pageIdxPlus in range(1, numPages + 1):
                page = pager.getCurrentPage()
                self.check_page(page, str(pageIdxPlus))

                do_result = pager.do('n')
                assert do_result.done
                assert not do_result.cancelled
                assert do_result.command == 'n'
                assert do_result.results is None

    def test_do_bad_next(self):
        pager = Pager([1], 1)

        with pytest.raises(Pager.EndReached):
            for _ in range(pager.getPageCount()[1] + 1):
                pager.do('n')

    def test_do_cancel(self):
        pager = Pager([1, 2], 1)

        do_result = pager.do('c')
        assert do_result.done
        assert do_result.cancelled
        assert do_result.command == 'c'
        assert do_result.results is None

    def test_bad_action(self):
        pager = Pager([1], 1)
        bad_action = "badaction1234"
        assert bad_action not in pager.getAllowedActions()
        with pytest.raises(Pager.BadAction):
            pager.do(bad_action)

    def test_do(self):
        pager = Pager([1, 2], 1)

        page = pager.getCurrentPage()
        assert '1' in page
        assert page['1'] == 1

        with pytest.raises(Pager.EndReached):
            pager.do('p')

        do_result = pager.do('n')
        assert do_result.done
        assert not do_result.cancelled
        assert do_result.command == 'n'
        assert do_result.results is None

        page = pager.getCurrentPage()
        assert '2' in page
        assert page['2'] == 2

        with pytest.raises(Pager.EndReached):
            pager.do('n')

        do_result = pager.do('p')
        assert do_result.done
        assert not do_result.cancelled
        assert do_result.command == 'p'
        assert do_result.results is None

        page = pager.getCurrentPage()
        assert '1' in page
        assert page['1'] == 1

        do_result = pager.do('c')
        assert do_result.done
        assert do_result.cancelled
        assert do_result.command == 'c'
        assert do_result.results is None

        with pytest.raises(Pager.BadAction):
            pager.do("badaction1234")
