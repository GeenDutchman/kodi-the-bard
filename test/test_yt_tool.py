import asyncio
import os
import pprint
from tempfile import TemporaryDirectory
import pytest

from bard.yt_tool import YT_tool

pytest_plugins = ('pytest_asyncio',)

shallow = pprint.PrettyPrinter(depth=1, compact=True)


class Test_YT_tool:

    @pytest.mark.slow
    @pytest.mark.asyncio
    async def test_download_async(self):
        with TemporaryDirectory() as directory:
            dl = YT_tool(directory)
            data = await dl.fetch_url_async(None, asyncio.get_event_loop(), "https://www.youtube.com/watch?v=dQw4w9WgXcQ",  download=True)
            data2 = await dl.prep(data)
            shallow.pprint(data)
            shallow.pprint(data2)
            assert 'title' in data
            assert 'Nyan' in data['title']
            shallow.pprint(os.listdir(directory))
            for filename in os.listdir(directory):
                assert 'Nyan' in filename

    @pytest.mark.slow
    @pytest.mark.asyncio
    async def test_nodownload_async(self):
        with TemporaryDirectory() as directory:
            dl = YT_tool(directory)
            data = await dl.fetch_url_async(None, asyncio.get_event_loop(), "https://www.youtube.com/watch?v=2yJgwwDcgV8", download=False)
            shallow.pprint(data)
            assert 'title' in data
            assert 'Nyan' in data['title']
            assert len(os.listdir(directory)) == 0
